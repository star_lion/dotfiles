" basics
syntax enable
set nocompatible
set encoding=utf-8

" show numberlines
set number

" show invisible characters
set list
set listchars=space:·,tab:▸-

" set invisible characters color
hi NonText ctermfg=236

" show commands as we type them
set showcmd

" highlight matching brackets
set showmatch

" search ignores case unless an uppercase letter used
set ignorecase
set smartcase

" show the first match as search strings are typed
set incsearch

" show all results of substitute command as it is typed
set inccommand=split

" highlight the search matches
set hlsearch

" use system clipboard
set clipboard=unnamedplus

" disable word wrap
set nowrap

" natural split directions
set splitright
set splitbelow

" set data directory
let b:datadirectory = has('win32') ? ($LOCALAPPDATA . '/nvim-data') : ($HOME . '/.nvim')

" move swapfiles out of project
let directory = b:datadirectory . '/swapfiles/'

" change diff colors for legibility
highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffDelete cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffText   cterm=bold ctermfg=10 ctermbg=88 gui=none guifg=bg guibg=Red

" map leader
let mapleader=';'

" map highlight clear
map <leader><leader> :noh<CR>

" map quit all
map <leader>q :qa<CR>

" map switch alternate file
map <leader>6 <c-^>

" map window command
map <leader>w <c-w>

" start automatic session saving and loading
" reference: https://stackoverflow.com/questions/1642611/how-to-save-and-restore-multiple-different-sessions-in-vim/47656092
function! MakeSession(overwrite)
  let b:sessiondir = b:datadirectory . '/sessions' . getcwd()
  if (filewritable(b:sessiondir) != 2)
    exe 'silent !mkdir -p ' b:sessiondir
    redraw!
  endif
  let b:filename = b:sessiondir . '/session.vim'
  if a:overwrite == 0 && !empty(glob(b:filename))
    return
  endif
  exe 'mksession! ' . b:filename
endfunction

function! LoadSession()
  let b:sessiondir = b:datadirectory . '/sessions' . getcwd()
  let b:sessionfile = b:sessiondir . '/session.vim'
  if (filereadable(b:sessionfile))
    exe 'source ' b:sessionfile
  else
    echo 'No session loaded.'
  endif
endfunction

" add automatons for when entering or leaving Vim
if(argc() == 0)
  au VimEnter * nested :call LoadSession()
  au VimLeave * :call MakeSession(1)
else
  au VimLeave * :call MakeSession(0)
endif
" end automatic session saving and loading

" unix only automated installation of vimplug
if !has('win32') && empty(glob($HOME . '/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo $HOME/.local/share/nvim/site/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ($HOME . '/.config/nvim/init.vim')
endif

" start plugins
call plug#begin(b:datadirectory . '/plugged')

" syntax
Plug 'sheerun/vim-polyglot'

" editing
Plug 'jiangmiao/auto-pairs'
Plug 'machakann/vim-sandwich'
Plug 'preservim/nerdcommenter'
Plug 'tpope/vim-sleuth'
Plug 'editorconfig/editorconfig-vim'

" ui
Plug 'airblade/vim-gitgutter'
Plug 'itchyny/vim-gitbranch'
Plug 'itchyny/lightline.vim'

" theme
Plug 'crusoexia/vim-monokai'

" os-dependent plugins
if has('win32')
    " navigation
    Plug 'ctrlpvim/ctrlp.vim'
else
    " navigation
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'

    " lsp
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
endif

call plug#end()
" end plugins

" theme
colorscheme monokai

" nerdcommenter jsx delimiters
let g:NERDCustomDelimiters={
	\ 'javascript': { 'left': '//', 'right': '', 'leftAlt': '{/*', 'rightAlt': '*/}' },
\}

" emmet mappings
map <leader>, <plug>(emmet-expand-abbr)
imap <leader>, <plug>(emmet-expand-abbr)

" vim-sandwich vim-surroud mappings
runtime macros/sandwich/keymap/surround.vim

" os-dependent plugin configs
if has('win32')
    " ctrlp root marker
    let g:ctrlp_root_markers = ['.editorconfig', 'docker-compose.yml']

    " ctrlp options
    let g:ctrlp_show_hidden = 1
    let g:ctrlp_custom_ignore = { 'dir': 'git$\|dist$\|build$\|submodules$\|node_modules$' }

    " ctrl mappings
    nmap <leader>f :CtrlP<CR>

    " lighline git configuration without coc
    let g:lightline = {
        \ 'active': {
        \   'left': [ [ 'mode', 'paste' ],
        \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
        \ },
        \ 'component_function': {
        \   'gitbranch': 'gitbranch#name'
        \ },
        \ }
else
    " fzf mappings
    nmap <leader>f :Files<CR>

    " fzf configuration
    let $FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git --exclude .vim || fdfind !*'
    let g:fzf_buffers_jump = 1

    " coc recommended vim settings
    set cmdheight=2
    set updatetime=300
    set shortmess+=c
    set signcolumn=yes

    " coc extensions
    let g:coc_global_extensions = ['coc-tsserver', 'coc-json', 'coc-css', 'coc-git',]

    " coc mappings
    inoremap <silent><expr> <TAB> pumvisible() ? "\<C-n>" : <SID>check_back_space() ? "\<TAB>" : coc#refresh()
    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    " ctrl-space to trigger completion.
    inoremap <silent><expr> <c-space> coc#refresh()

    " Use <cr> to comfirm completion.
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

    " Keys for gotos.
    " Add any filetype with an lsp to this au.
    autocmd FileType c,cpp nmap <silent> gd <Plug>(coc-declaration)
    nmap <leader>gd <Plug>(coc-definition)
    nmap <leader>gr <Plug>(coc-references)

    " coc helper functions
    function! s:check_back_space() abort
    	let col = col('.') - 1
    	return !col || getline('.')[col - 1]  =~# '\s'
    endfunction

    " lighline git configuration
    let g:lightline = {
    	\ 'active': {
    	\   'left': [ [ 'mode', 'paste' ],
    	\             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
    	\ },
    	\ 'component_function': {
    	\   'gitbranch': 'gitbranch#name',
    	\   'cocstatus': 'coc#status'
    	\ },
    	\ }

    " use autocmd to force lightline update
    autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()
endif

" " vundle
" filetype off
" set rtp+=~/.nvim/bundle/Vundle.vim
" call vundle#begin()

" " utility
" Plugin 'vundlevim/vundle.vim'
" Plugin 'editorconfig/editorconfig-vim'
" Plugin 'ctrlpvim/ctrlp.vim'
" Plugin 'terryma/vim-multiple-cursors'
" Plugin 'airblade/vim-gitgutter'
" Plugin 'tpope/vim-surround'
" Plugin 'vim-airline/vim-airline'
" Plugin 'ciaranm/detectindent'
"
" " syntax
" Plugin 'mxw/vim-jsx'
" Plugin 'pangloss/vim-javascript'
" Plugin 'digitaltoad/vim-pug'
" Plugin 'wavded/vim-stylus'
" Plugin 'groenewege/vim-less'
"
" call vundle#end()
" filetype plugin indent on

" " always show powerline
" set laststatus=2
"
" " allow JSX in normal JS files
" let g:jsx_ext_required = 0
"
" " ctrlp root marker
" let g:ctrlp_root_markers = ['.editorconfig', 'docker-compose.yml']
"
" " ctrlp use silver searcher - reference: https://robots.thoughtbot.com/faster-grepping-in-vim
" if executable('ag')
"   " use ag in CtrlP for listing files. Lightning fast and respects .gitignore
"   let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'
"
"   " ag is fast enough that CtrlP doesn't need to cache
"   let g:ctrlp_use_caching = 0
" else
"   let g:ctrlp_show_hidden = 1
"   let g:ctrlp_custom_ignore = { 'dir': 'git$\|build$\|node_modules$' }
" endif
"
