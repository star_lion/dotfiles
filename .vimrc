" basics
syntax enable
set nocompatible
set encoding=utf-8

" show numberlines
set number

" show invisible characters
set list
set listchars=space:·,tab:▸-

" show commands as we type them
set showcmd

" highlight matching brackets
set showmatch

" search ignores case unless an uppercase letter used
set ignorecase
set smartcase

" show the first match as search strings are typed
set incsearch

" NOTE: replace bottom neovim functionality with a plugin (traces)
" show all results of substitute command as it is typed
" set inccommand=split

" highlight the search matches
set hlsearch

" use system clipboard
set clipboard=unnamed

" disable word wrap
set nowrap

" natural split directions
set splitright
set splitbelow

" set data directory
let b:datadirectory = $HOME . '/.vim'


" move swapfiles out of project
if !isdirectory(b:datadirectory . '/swapfiles/')
    call mkdir(b:datadirectory . '/swapfiles/', 'p')
endif
let &directory = b:datadirectory . '/swapfiles/'

" " change diff colors for legibility
highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffDelete cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffText   cterm=bold ctermfg=10 ctermbg=88 gui=none guifg=bg guibg=Red

" map leader
let mapleader=';'
"
" map highlight clear
map <leader><leader> :noh<CR>
"
" map quit all
map <leader>q :qa<CR>

" map switch alternate file
map <leader>6 <c-^>

" map window command
map <leader>w <c-w>

" " start automatic session saving and loading
" " reference: https://stackoverflow.com/questions/1642611/how-to-save-and-restore-multiple-different-sessions-in-vim/47656092
" function! MakeSession(overwrite)
"   let b:sessiondir = b:datadirectory . '/sessions' . getcwd()
"   if (filewritable(b:sessiondir) != 2)
"     exe 'silent !mkdir -p ' b:sessiondir
"     redraw!
"
"   endif
"   let b:filename = b:sessiondir . '/session.vim'
"   if a:overwrite == 0 && !empty(glob(b:filename))
"     return
"   endif
"   exe 'mksession! ' . b:filename
" endfunction
"
" function! LoadSession()
"   let b:sessiondir = b:datadirectory . '/sessions' . getcwd()
"   let b:sessionfile = b:sessiondir . '/session.vim'
"   if (filereadable(b:sessionfile))
"     exe 'source ' b:sessionfile
"   else
"     echo 'No session loaded.'
"   endif
" endfunction
"
" " add automatons for when entering or leaving Vim
" if(argc() == 0)
"   au VimEnter * nested :call LoadSession()
"   au VimLeave * :call MakeSession(1)
" else
"   au VimLeave * :call MakeSession(0)
" endif
" " end automatic session saving and loading

" start plugins
call plug#begin(b:datadirectory . '/plugged')

" syntax
Plug 'sheerun/vim-polyglot'

" editing
Plug 'jiangmiao/auto-pairs'
Plug 'machakann/vim-sandwich'
Plug 'preservim/nerdcommenter'
Plug 'tpope/vim-sleuth'
Plug 'mattn/emmet-vim'
Plug 'editorconfig/editorconfig-vim'

" session management
Plug 'thaerkh/vim-workspace'

" ui
Plug 'airblade/vim-gitgutter'
Plug 'itchyny/vim-gitbranch'
Plug 'itchyny/lightline.vim'

" theme
Plug 'crusoexia/vim-monokai'

" os-dependent plugins
if has('win32unix')
    " navigation
    Plug 'ctrlpvim/ctrlp.vim'
else
    " navigation
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'

    " lsp
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
endif

call plug#end()

" set theme
colorscheme monokai

" set invisible characters color
hi NonText ctermfg=8
hi SpecialKey ctermfg=8

" nerdcommenter jsx delimiters
let g:NERDCustomDelimiters={
    \ 'javascript': { 'left': '//', 'right': '', 'leftAlt': '{/*', 'rightAlt': '*/}' },
\}

" emmet mappings
map <leader>, <plug>(emmet-expand-abbr)
imap <leader>, <plug>(emmet-expand-abbr)

" vim-sandwich vim-surroud mappings
runtime macros/sandwich/keymap/surround.vim

" vim-workspace configuration
let g:workspace_autocreate = 1
let g:workspace_session_disable_on_args = 1
let g:workspace_session_directory = b:datadirectory . '/sessions/'
let g:workspace_undodir = b:datadirectory . '/undodir/'
let g:workspace_persist_undo_history = 0
let g:workspace_autosave_always = 0
let g:workspace_autosave = 0


" os-dependent plugin configs
if has('win32unix')
    " ctrlp root marker
    let g:ctrlp_root_markers = ['.editorconfig', 'docker-compose.yml']

    " ctrlp options
    let g:ctrlp_show_hidden = 1
    let g:ctrlp_custom_ignore = { 'dir': 'git$\|dist$\|build$\|submodules$\|node_modules$' }

    " ctrl mappings
    nmap <leader>f :CtrlP<CR>

    " lighline git configuration without coc
    let g:lightline = {
        \ 'active': {
        \   'left': [ [ 'mode', 'paste' ],
        \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
        \ },
        \ 'component_function': {
        \   'gitbranch': 'gitbranch#name'
        \ },
        \ }
else
    " fzf mappings
    nmap <leader>f :Files<CR>

    " fzf configuration
    "let $FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git --exclude .vim || fdfind !*'
    let $FZF_DEFAULT_COMMAND='fdfind --type f --hidden --follow --exclude .git --exclude .vim'
    let g:fzf_buffers_jump = 1

    " coc recommended vim settings
    set cmdheight=2
    set updatetime=300
    set shortmess+=c
    set signcolumn=yes

    " coc extensions
    let g:coc_global_extensions = ['coc-tsserver', 'coc-json', 'coc-css', 'coc-git',]

    " coc mappings
    inoremap <silent><expr> <TAB> pumvisible() ? "\<C-n>" : <SID>check_back_space() ? "\<TAB>" : coc#refresh()
    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    " ctrl-space to trigger completion.
    inoremap <silent><expr> <c-space> coc#refresh()

    " Use <cr> to comfirm completion.
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

    " Keys for gotos.
    " Add any filetype with an lsp to this au.
    autocmd FileType c,cpp nmap <silent> gd <Plug>(coc-declaration)
    nmap <leader>gd <Plug>(coc-definition)
    nmap <leader>gr <Plug>(coc-references)

    " coc helper functions
    function! s:check_back_space() abort
        let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~# '\s'
    endfunction

    " lighline git configuration
    let g:lightline = {
        \ 'active': {
        \   'left': [ [ 'mode', 'paste' ],
        \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
        \ },
        \ 'component_function': {
        \   'gitbranch': 'gitbranch#name',
        \   'cocstatus': 'coc#status'
        \ },
        \ }

    " use autocmd to force lightline update
    autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()
endif

" https://github.com/microsoft/WSL/issues/4440#issuecomment-638884035
" WSL yank support
if system('uname -r') =~ "microsoft"
  augroup Yank
  autocmd!
  autocmd TextYankPost * :call system('/mnt/c/windows/system32/clip.exe ',@")
  augroup END
endif
