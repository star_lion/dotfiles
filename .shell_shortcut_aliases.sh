alias v='vim'
alias todos='rg "todo|Todo|TODO"'
alias itree='tree --prune -I $(cat $DOTFILES_PATH/.gitignore | egrep -v "^#.*$|^[[:space:]]*$" | tr "\\n" "|")' #https://coderwall.com/p/wgq89a/use-tree-with-gitignore

if [[ $(uname) != "Darwin" ]]; then
  alias l='LC_COLLATE="C" ls -alh --group-directories-first --color'
elif [[ $(uname) == "Darwin" ]] && (( ${+commands[gls]} )); then
  alias l='LC_COLLATE="C" gls -alh --group-directories-first --color'
else
  alias l='LC_COLLATE="C" ls -alh --color'
fi

if [[ $(uname) != "Darwin" ]] && type 'xclip' &> /dev/null; then
  alias pbcopy='xclip -selection clipboard'
  alias pbpaste='xclip -selection clipboard -o'
fi

if [[ $(uname) != "Darwin" ]] && type 'xsel' &> /dev/null; then
  alias pbcopy='xsel --clipboard --input'
  alias pbpaste='xsel --clipboard --output'
fi
