# Manual Steps
- install git as sdk for better bash tooling (https://medium.com/@saml/setting-up-a-light-weight-linux-like-non-wsl-terminal-environment-on-windows-59a583c958ed)
- enable "Developer mode" in Windows 10 settings to allow non-admin git bash to create symlinks (https://github.community/t/git-bash-symbolic-links-on-windows/522/11)
- install tmux for git bash (https://gist.github.com/lhsfcboy/f5802a5985a1fe95fddb43824037fe39)
  - dotfiles repo must have LF line endings, CRLF line endings will cause config loading to fail
- in device manager, require "magic packet" for Wake on Lan device, to prevent miscellaneous wakes
- linux dual boot
  - disable fast startup (https://wiki.archlinux.org/index.php/Dual_boot_with_Windows#Fast_Startup_and_hibernation)
  - set time mode (https://wiki.archlinux.org/index.php/System_time#UTC_in_Windows)
- set timezone
- copy "Power Options" shortcut to desktop/taskbar
- set eartrumpet hotkey to ctrl + alt + v
- remove extra taskbar shortcuts, scripting this seems advised against 
- remove extra desktop shortcuts, this can be scripted
- sort desktop by type
- nvm (node version manager)
- optional
  - guiscrcpy for android convenience


# References
- nvim mintty TUI issue (https://github.com/neovim/neovim/issues/6751)
