#Requires -RunAsAdministrator
choco install -y `
	7zip `
	vlc `
	eartrumpet `
	brave `
	chromium-stable `
	google-backup-and-sync `
	neovim `
	vscode `
	SublimeText3 `
	python3 `
	fd `
	vcredist2015 `
	vcredist2017 `
	autohotkey `
	spotify `
