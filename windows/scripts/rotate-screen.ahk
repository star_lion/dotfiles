SysGet, LeftMon, Monitor, 3
MonWidth := LeftMonRight - LeftMonLeft ; 1920 or 1080 depending on orientation

if (MonWidth = 1080) {
    DisplayOptions = /rotate 0 /position 1920 0
    TargetOrientation = Landscape
} else if (MonWidth = 1920) {
    DisplayOptions = /rotate 90 /position 1920 -449
    TargetOrientation = Portrait
}

;MsgBox, %TargetOrientation%
Run, C:\Users\%A_UserName%\Downloads\display\display64.exe /device 3 %DisplayOptions%,, Hide

