; reference: https://www.autohotkey.com/docs/commands/MouseGetPos.htm#ExWatchCursor
#Persistent
SetTimer, WatchCursor, 500
return

WatchCursor:
MouseGetPos, , , id, control
WinGetTitle, title, ahk_id %id%
WinGetClass, class, ahk_id %id%
WinGet, process, ProcessName, ahk_id %id%
ToolTip, ahk_id %id%`nahk_class %class%`n%title%`nProcess: %process%`nControl: %control%
return
