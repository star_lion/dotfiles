#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn ; Enable warnings to assist with detecting common errors.
#SingleInstance force ; Replace old instance automatically without warning dialogue.

setTitleMatchMode,2 ; Match any part of window title

; global hotkeys
<^<!t::Run, C:\git-sdk-64\git-bash.exe, C:/Users/%A_UserName%/Desktop/projects
<^<!a::Winset, Alwaysontop, , A
Launch_Mail::DllCall("PowrProf\SetSuspendState", "int", 0, "int", 1, "int", 0)

; scoped hotkeys
#If WinActive("Genshin Impact")
XButton1::Escape
XButton2::MButton
CapsLock::LButton
return

#If WinActive("Modded Slay the Spire")
`::RButton
CapsLock::LButton
return

#If WinActive("Slay the Spire")
CapsLock::LButton
return

#If WinActive("StateOfDecay2")
CapsLock::LButton
return

#If WinActive("Langrisser")
RButton::Escape
XButton1::Escape
CapsLock::LButton
return

#If WinActive("Unexplored")
XButton2::t
return

#If WinActive("Project Zomboid")
XButton1::F2 ; pause
XButton2::F3 ; 1x speed
MButton::F4 ; 2x speed
CapsLock::LButton
return

#If WinActive("ahk_exe OxygenNotIncluded.exe")
XButton1::\ ; copy settings
XButton2::b ; copy
MButton::p ; priority
return

#If WinActive("Generation Zero")
XButton1::I ; copy settings
return

#If WinActive("No Man's Sky")
XButton1::G ; Switch mode
XButton2::F ; Vysor
return


#If WinActive("melonDS")
`;::
    KeyDown := !KeyDown
    If KeyDown
        SendInput {4 down}
    Else
        SendInput {4 up}
    return
[::+F1
=::F1
q::
    MouseClick, left, 1200, 520
    return
w::
    MouseClick, left, 1600, 520
    return
return
