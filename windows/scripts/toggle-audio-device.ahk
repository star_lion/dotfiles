;{0.0.0.00000000}.{55fd277d-54e1-4fc3-9f23-6160dc016259}
SpeakersHighDefinitionAudioDevice={0.0.0.00000000}.{55fd277d-54e1-4fc3-9f23-6160dc016259}
HeadsetEarphoneMicrosoftLifeChat={0.0.0.00000000}.{3c9dd3e1-dcde-42d2-b333-42bb824555af}
CurrentDeviceID:=ComObjCreate("WScript.Shell").Exec("powershell -Command Get-AudioDevice -Playback | select -ExpandProperty ID").StdOut.ReadAll()

if (InStr(CurrentDeviceID, SpeakersHighDefinitionAudioDevice) != 0)
    TargetDeviceID:=HeadsetEarphoneMicrosoftLifeChat
else
    TargetDeviceID:=SpeakersHighDefinitionAudioDevice

Run, powershell -Command Set-AudioDevice -ID '%TargetDeviceID%',,Hide

