ProcessExist(Name){
    Process,Exist,%Name%
    return Errorlevel
}

If !ProcessExist("Jellyfin.Windows.Tray.exe")
    Run *RunAs C:\Program Files\Jellyfin\Server\Jellyfin.Windows.Tray.exe

Runwait, taskkill /im caddy.exe /f
Run, caddy start --config C:\Users\%A_UserName%\Desktop\services\jellyfin-reverse-proxy\Caddyfile
