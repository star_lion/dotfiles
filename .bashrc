# require DOTFILES_PATH
: ${DOTFILES_PATH:?"DOTFILES_PATH environment variable needs to be non-empty"}

# editor
export VISUAL='vim'
export EDITOR=$VISUAL

# file aliases
alias re-source='source $HOME/.bash_profile'
alias bash_profile='$EDITOR $HOME/.bash_profile'
alias bash_profile2='$EDITOR $DOTFILES_PATH/.bash_profile'
alias nvimrc='$EDITOR $DOTFILES_PATH/.config/nvim/init.vim'
alias vimrc='$EDITOR $DOTFILES_PATH/.vimrc'
alias tmuxconf='$EDITOR $DOTFILES_PATH/.tmux.conf'

# shortcut aliases
alias ..='cd ..'
alias ...='cd ../..'
source $DOTFILES_PATH/.shell_shortcut_aliases.sh

# program aliases
source $DOTFILES_PATH/aliases/.git_aliases
source $DOTFILES_PATH/aliases/.npm_aliases
source $DOTFILES_PATH/aliases/.pnpm_aliases
source $DOTFILES_PATH/aliases/.tmux_aliases

# functions
source $DOTFILES_PATH/.shell_functions.sh

