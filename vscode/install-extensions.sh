code --install-extension vscodevim.vim
code --install-extension ryuta46.multi-command
code --install-extension EditorConfig.EditorConfig
code --install-extension maximetinu.identical-sublime-monokai-csharp-theme-colorizer
