# color display
terminal-colors () {
  for i in {0..255} ; do
    printf "\x1b[48;5;%sm%3d\e[0m " "$i" "$i"
    if (( i == 15 )) || (( i > 15 )) && (( (i-15) % 6 == 0 )); then
      printf "\n";
    fi
  done
}

# cross-platform open
o () {
  # if windows - https://superuser.com/questions/246825/open-file-from-the-command-line-on-windows
  # if mac - probably just alias open
  # if linux with xdg-open
  if ! type 'xdg-open' > /dev/null; then
    echo 'xdg-open could not be found'
  else
    nohup xdg-open "$*" > /dev/null 2>&1
  fi
  # else error: unsupported environment
}

