# require DOTFILES_PATH
: ${DOTFILES_PATH:?"DOTFILES_PATH environment variable needs to be non-empty"}

# editor
export VISUAL="vim"
export EDITOR=$VISUAL

# zsh options
setopt auto_cd
setopt correct

# file aliases
alias re-source="source $HOME/.zshrc"
alias profile="$EDITOR $HOME/.zprofile"
alias rc="$EDITOR $HOME/.zshrc"
alias rc2="$EDITOR $DOTFILES_PATH/.zshrc"
alias vimrc="$EDITOR $DOTFILES_PATH/.vimrc"
alias tmuxconf="$EDITOR $DOTFILES_PATH/.tmux.conf"

# shortcut aliases
alias ..="cd .."
alias ...="cd ../.."
source $DOTFILES_PATH/.shell_shortcut_aliases.sh

# program aliases
source $DOTFILES_PATH/aliases/.aws_aliases
source $DOTFILES_PATH/aliases/.docker_aliases
source $DOTFILES_PATH/aliases/.git_aliases
source $DOTFILES_PATH/aliases/.npm_aliases
source $DOTFILES_PATH/aliases/.tmux_aliases

# functions
source $DOTFILES_PATH/.shell_functions.sh

